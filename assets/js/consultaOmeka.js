/**
 * Created by Breno Moura on 09/11/2020.
 */
var itens;
const itensPage = 25;
window.itens = itens;
window.itensPage = itensPage;

document.addEventListener('readystatechange', () => {
    if (document.readyState == 'complete') {
        mountPage();
    }
});

/**
 * Função assincrona que chama os itens e monta a pagina
 * @returns {Promise.<void>}
 */
async function mountPage () {

    // wait 3 seconds
    // await new Promise((resolve, reject) => setTimeout(resolve, 3000));
    var loading = document.getElementById("loading");
    loading.style.display = "block";
    await getItens().then(result => {
        totalItens = result.length;
        populateHtml(result,totalItens);
    }).catch (function (e) {
            console.log("error: ");
            console.log(e);
            loading.style.display = "none";
    });
    loading.style.display = "none";
}

/**
 * Função que popula a tela com os itens da api e monta a paginação passando os itens e o total de itens
 * @param result
 * @param totalItens
 */
function populateHtml (result,totalItens) {
    itens = result;
    const totalPages = Math.ceil(totalItens/itensPage);
    mountPagination(totalPages);
    const page = 1;
    showItems(page);
}

/**
 * Função que pega os itens da api de forma ordenada e retorna a resposta em json
 * @returns {Promise.<*>}
 */
async function getItens () {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "http://swojs.ibict.br/portal/api/items?item_set_id=309&page&sort_by=dcterms:title&sort_order=asc";
    return fetch (proxyurl + url).then(response => response.json()).catch(error => console.log(error));
}

/**
 * Função que monta a lista de itens por pagina
 * @param page
 */
function showItems (page) {
    const pageItens = document.querySelectorAll('.pageItem');

    pageItens.forEach(function (item,index) {
        if (item.classList.contains("active")){
            item.classList.remove("active");
        }
    });

    var active = document.getElementById(`page_${page}`);
    active.classList.add("active");

    var listDiv = document.getElementById('items');
    listDiv.innerHTML = '';
    var ul = document.createElement('ul');
    let start = (page*itensPage) - itensPage;
    let end = (page*itensPage);
    for (var i = start; i < end; i++ ) {
        if(itens[i]['o:title']){
            var li = document.createElement('li');
            li.innerHTML = `<button class='button_link' onclick='showItem(${JSON.stringify(itens[i])})' data-toggle="modal" data-target="#modal-item" >${(JSON.stringify(itens[i]['o:title'])).replace(/['"]+/g, '')}<span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span><sgnw-sign fsw='${JSON.stringify(itens[i]['oc:transcription'][0]['@value']).replace(/['"]+/g, '')}' styling="-Z1" /></button>`;  // Use innerHTML to set the text
            ul.appendChild(li);
        }
    }
    listDiv.appendChild(ul);
}

/**
 * Função que mostra os detalhes do item
 * @param item
 */
function showItem (item) {
    document.getElementById('label-item').innerHTML = JSON.stringify(item['o:title']).replace(/['"]+/g, '');
    var htmlContent = '<table>';
    htmlContent += `<tr><td>Transcrição (oc:transcription): </td><td><sgnw-sign fsw='${JSON.stringify(item['oc:transcription'][0]['@value']).replace(/['"]+/g, '')}' styling="-Z1" /> ${JSON.stringify(item['oc:transcription'][0]['@value']).replace(/['"]+/g, '')} </td></tr>`;
    htmlContent += `<tr><td>Idioma origem: </td><td> ${JSON.stringify(item['http://www.lingvoj.org/ontology:originalLanguage'][0]['@value']).replace(/['"]+/g, '')} </td></tr>`;
    htmlContent += `<tr><td>Idioma destino: </td><td> ${JSON.stringify(item['http://www.lingvoj.org/ontology:targetLanguage'][0]['@value']).replace(/['"]+/g, '')} </td></tr>`;
    htmlContent += `<tr><td>Url Video: </td><td> <a href="${JSON.stringify(item['http://purl.org/linguistics/gold:signedRealization'][0]['display_title']).replace(/['"]+/g, '')}">${JSON.stringify(item['http://purl.org/linguistics/gold:signedRealization'][0]['display_title']).replace(/['"]+/g, '')}</a> </td></tr>`;
    htmlContent += `<tr><td>Vídeo: </td><td> <video class="video" src="${JSON.stringify(item['http://purl.org/linguistics/gold:signedRealization'][0]['display_title']).replace(/['"]+/g, '')}" controls autoplay muted loop/> </td></tr>`;
    htmlContent += '</table>';
    document.getElementById('item').innerHTML = htmlContent;
}

/**
 * Função que monta a paginação de acordo com a quantidade de paginas
 * @param pagesLength
 */
function mountPagination (pagesLength) {
    var htmlContent = "";
    for (var i = 1; i <= pagesLength; i++) {
        htmlContent += `<button id='page_${i}' class='button_link pageItem' onclick='showItems(${i})'>${i}</button><span>&nbsp;</span>`;
    }
    document.getElementById('paginationDiv').innerHTML = htmlContent;
}


/**
 * Monta o select
 * @param value
 */
async function consultaItens(value) {
    try {
        console.log(value);
        console.log(itens.some(entry => JSON.stringify(entry['o:title']).replace(/['"]+/g, '') == value));
        let teste = toLowercase(value);
        console.log(itens.filter(item => JSON.stringify(item['o:title']).replace(/['"]+/g, '').toLowerCase(teste).indexOf() > -1));
        if(itens.some(entry => JSON.stringify(entry['o:title']).replace(/['"]+/g, '') == value)) {
            console.log(JSON.stringify(entry));
        }
        // itens.map(function(busca, i) {
        //     JSON.stringify(busca['o:title']).replace(/['"]+/g, '');
        //     console.log(JSON.stringify(busca['o:title']).replace(/['"]+/g, ''));
        //     if (!indTotGeral.indicador.some(entry => JSON.stringify(entry['o:title']).replace(/['"]+/g, '') == busca)) {
        //
        //     }
        //     item += `<li data-active='false'
        //                         class='busca-${busca.id_indicador} listagem-opcao-radio animated fadeInLeft'
        //                         data-div='#listaIndicadorContorno' name='ind-${busca.id_indicador}'
        //                         data-dim='${nomeDiv}' value='${divDestino}' id='${JSON.stringify(busca)}'
        //                         onclick=selecionarIndMapa(this)><i class='${squareIcon}'></i> ${busca.indicador}</li>`;
        // });
        // $(`#div-${divDestino}`).html(item);
        // total = 0;
    } catch (error) {
        console.log('Error:', error);
    }
}
