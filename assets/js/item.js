/**
 * Created by Breno Moura on 10/11/2020.
 */

/**
 * Função assincrona que chama os itens e monta a pagina
 * @returns {Promise.<void>}
 */
async function mountPage (item) {
    var content = document.getElementById('filtro-item');
    if (content.style.display === "block") {
        content.style.display = "none";
    } else {
        content.style.display = "block";
        const url = `http://swojs.ibict.br/portal/api/items?property[0][property]=10&property[0][type]=eq&property[0][text]=${item}`;
        await getItens(url).then(result =>{
            showItem(result);
        }).catch (function (e) {
            console.log("error: ");
            console.log(e);
            // loading.style.display = "none";
        });
    }
}

/**
 * Função que pega os itens da api de forma ordenada e retorna a resposta em json
 * @returns {Promise.<*>}
 */
async function getItens (url) {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    return fetch (proxyurl + url).then(response => response.json()).catch(error => console.log(error));
}

/**
 * Função que monta o html collapse com os valores do item
 * @param item
 */
function showItem (item) {
    var htmlContent = '<table>';
    let termos;

    item.forEach(function (el,index) {
        if(el.hasOwnProperty('http://www.lingvoj.org/ontology:translatedResource')){
            termos = terms('http://www.lingvoj.org/ontology:translatedResource',el);
            htmlContent += `<tr><td>Termo em Português: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://lexvo.org/ontology:lexicalCategory')){
            termos = terms('http://lexvo.org/ontology:lexicalCategory',el);
            htmlContent += `<tr><td>Classe gramatical: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://purl.org/linguistics/gold:hasMeaning')){
            htmlContent += `<tr><td>Definição em libras: </td><td><video class="video" src="${JSON.stringify(el['http://purl.org/linguistics/gold:hasMeaning'][0]['display_title']).replace(/['"]+/g, '')}" controls autoplay muted loop/></td></tr>`;
        }

        if(el.hasOwnProperty('http://lexvo.org/ontology:means')){
            termos = terms('http://lexvo.org/ontology:means',el);
            htmlContent += `<tr><td>Definição em português: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('oc:text')){
            termos = terms('oc:text',el);
            htmlContent += `<tr><td>Utilização do termo em uma frase: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://purl.org/linguistics/gold:hasMorphologicalConstituent')){
            termos = terms('http://purl.org/linguistics/gold:hasMorphologicalConstituent',el);
            htmlContent += `<tr><td>Processo morfológico: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://lexvo.org/ontology:variant')){
            termos = terms('http://lexvo.org/ontology:variant',el);
            htmlContent += `<tr><td>Variante: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://www.lingvoj.org/ontology:interpreter')){
            termos = terms('http://www.lingvoj.org/ontology:interpreter',el);
            htmlContent += `<tr><td>Intérprete: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://www.lingvoj.org/ontology:translator')){
            termos = terms('http://www.lingvoj.org/ontology:translator',el);
            htmlContent += `<tr><td>Tradutor: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://lexvo.org/ontology:variant')){
            termos = terms('http://lexvo.org/ontology:variant',el);
            htmlContent += `<tr><td>Variante: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('dcterms:description')){
            termos = terms('dcterms:description',el);
            htmlContent += `<tr><td>Descrição: </td><td>${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://purl.org/linguistics/gold:writtenRealization')){
            htmlContent += `<tr><td>Sinal-termo: </td><td><sgnw-sign fsw='${JSON.stringify(el['http://purl.org/linguistics/gold:writtenRealization'][0]['@value']).replace(/['"]+/g, '')}' styling="-Z1" /> ${JSON.stringify(el['http://purl.org/linguistics/gold:writtenRealization'][0]['@value']).replace(/['"]+/g, '')} </td></tr>`;
        }

        if(el.hasOwnProperty('http://www.lingvoj.org/ontology:originalLanguage')){
            termos = terms('http://www.lingvoj.org/ontology:originalLanguage',el);
            htmlContent += `<tr><td>Idioma origem: </td><td> ${termos}</td></tr>`;
        }

        if(el.hasOwnProperty('http://www.lingvoj.org/ontology:targetLanguage')){
            termos = terms('http://www.lingvoj.org/ontology:targetLanguage',el);
            htmlContent += `<tr><td>Idioma destino: </td><td> ${termos} </td></tr>`;
        }

        if(el.hasOwnProperty('http://purl.org/linguistics/gold:signedRealization')){
            htmlContent += `<tr><td>Url Video: </td><td> <a href="${JSON.stringify(el['http://purl.org/linguistics/gold:signedRealization'][0]['display_title']).replace(/['"]+/g, '')}">${JSON.stringify(el['http://purl.org/linguistics/gold:signedRealization'][0]['display_title']).replace(/['"]+/g, '')}</a> </td></tr>`;
            htmlContent += `<tr><td>Vídeo: </td><td> <video class="video" src="${JSON.stringify(el['http://purl.org/linguistics/gold:signedRealization'][0]['display_title']).replace(/['"]+/g, '')}" controls autoplay muted loop/> </td></tr>`;
        }
    });
    htmlContent += '</table>';
    document.getElementById('filtro-item').innerHTML = htmlContent;
}

/**
 * Função que retorna os valores ou o valor do termo formatado
 * @param index
 * @param el
 * @returns {string}
 */
function terms (index,el){
    let termos = "";
    var sizeTerm = el[index].length;
    if(sizeTerm > 0) {
        el[index].forEach(function(value,index){
            console.log(value);
            if(value.hasOwnProperty('@value')){
                termos += decodeURI(JSON.stringify(value['@value'].replaceAll('\n','<br/>')).replace(/['"]+/g, ''));
                termos += index < (sizeTerm - 1) ? ', ' : '';
            }
        });
    } else {
        if(el[index].hasOwnProperty('@value')){
            termos = el[index]['@value'].replaceAll('\n','<br/>').replace(/['"]+/g, '');
        }
    }

    return termos;
}
